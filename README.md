# Flectra Community / l10n-switzerland

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_ch_isrb](l10n_ch_isrb/) | 2.0.1.0.0| Switzerland - ISR with Bank
[server_env_ebill_paynet](server_env_ebill_paynet/) | 2.0.1.0.0| Server environment for Ebill Paynet
[l10n_ch_base_bank](l10n_ch_base_bank/) | 2.0.1.0.1| Types and number validation for swiss electronic pmnt. DTA, ESR
[l10n_ch_account_tags](l10n_ch_account_tags/) | 2.0.1.0.0| Switzerland Account Tags
[l10n_ch_isr_payment_grouping](l10n_ch_isr_payment_grouping/) | 2.0.1.0.0| Extend account to ungroup ISR
[l10n_ch_invoice_reports](l10n_ch_invoice_reports/) | 2.0.1.2.0| Extend invoice to add ISR/QR payment slip
[ebill_paynet](ebill_paynet/) | 2.0.1.1.0|         Paynet platform bridge implementation
[ebill_paynet_customer_free_ref](ebill_paynet_customer_free_ref/) | 2.0.1.0.1| Glue module: ebill_paynet and sale_order_customer_free_ref
[l10n_ch_states](l10n_ch_states/) | 2.0.1.0.0| Switzerland Country States
[l10n_ch_isr_payment_grouping](l10n_ch_isr_payment_grouping/) | 2.0.1.0.0| Extend account to ungroup ISR
[server_env_ebill_paynet](server_env_ebill_paynet/) | 2.0.1.0.0| Server environment for Ebill Paynet
[l10n_ch_invoice_reports](l10n_ch_invoice_reports/) | 2.0.1.0.0| Extend invoice to add ISR/QR payment slip
[l10n_ch_isrb](l10n_ch_isrb/) | 2.0.1.0.0| Switzerland - ISR with Bank


